# Home Screen App Serasa

Copia da tela Home do aplicativo Serasa. O objetivo da construção do App foi de uma forma inicial, colocar em prática o conhecimento de react native e usar as libs utilizadas pela equipe RN.

## Requisitos

* yarn 1.22.5
* react-native-cli 2.0.1
* Android Studio
* node 12.16.1

## Instalação

Considerando que o ambiente de desenvolvimente ja estaje configurado com android studio, possibilitando o uso do emulador. Basta clonar este repositório com o comando:

```sh 
    git clone https://martinsLucas@bitbucket.org/martinsLucas/home-screen.git 
```

Navegue até a pasta do projeto:

```sh
    cd app-serasa-clone\homeAppSerasa
```
Faça a instalação das dependências:

```sh
    yarn
```

Após a instalação das dependências, basta iniciar o react-native com o android:

```sh
    react-native run-android
```