import React from 'react';
import 'react-native-gesture-handler';

import {Text} from 'react-native';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';

import LoggedTabs from './LoggedTabs';
import StoryScreen from '../screens/StoryScreen';

export type MainStackParamList = {
  LoggedStack: {};
  StoryScreen: {};
};

const Stack = createStackNavigator<MainStackParamList>();

const MainStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="LoggedStack"
        component={LoggedTabs}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="StoryScreen"
        component={StoryScreen}
        options={{
          headerShown: false,
          ...TransitionPresets.ModalSlideFromBottomIOS,
          gestureEnabled: true,
          gestureDirection: 'vertical',
        }}
      />
    </Stack.Navigator>
  );
};

export default MainStack;
