import React from 'react';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import Home from '../screens/Home';
import ServiceScreen from '../screens/ServicesScreen';
import Help from '../screens/Help';
import Profile from '../screens/Profile';
import IconTab from '../components/IconTab';
import {
  faHome,
  faUser,
  faQuestionCircle,
  faThLarge,
} from '@fortawesome/free-solid-svg-icons';

import TextTab from '../components/TextTab';

export type LoggedStackParamList = {
  Home: {};
  ServiceScreen: {};
  Help: {};
  Profile: {};
};

const Tab = createBottomTabNavigator<LoggedStackParamList>();

const LoggedTabs = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({focused}) => (
            <IconTab selected={focused} icon={faHome} />
          ),
          tabBarLabel: ({focused}) =>
            focused ? <></> : <TextTab>Home</TextTab>,
        }}
      />
      <Tab.Screen
        name="ServiceScreen"
        component={ServiceScreen}
        options={{
          tabBarIcon: ({focused}) => (
            <IconTab selected={focused} icon={faThLarge} />
          ),
          tabBarLabel: ({focused}) =>
            focused ? <></> : <TextTab>Serviços</TextTab>,
        }}
      />
      <Tab.Screen
        name="Help"
        component={Help}
        options={{
          tabBarIcon: ({focused}) => (
            <IconTab selected={focused} icon={faQuestionCircle} />
          ),
          tabBarLabel: ({focused}) =>
            focused ? <></> : <TextTab>Dúvidas</TextTab>,
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: ({focused}) => (
            <IconTab selected={focused} icon={faUser} />
          ),
          tabBarLabel: ({focused}) =>
            focused ? <></> : <TextTab>Perfil</TextTab>,
        }}
      />
    </Tab.Navigator>
  );
};

export default LoggedTabs;
