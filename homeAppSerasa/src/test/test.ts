const imagemExemplo = '';

export const storyData = [
  {
    key: 'lno-50',
    title: 'Metade da dívida!',
    viewed: false,
    time: 7000,
    thumbnail: {
      title: 'Imagem com um boleto sendo rasgado ao meio',
      x1: imagemExemplo,
      x2: imagemExemplo,
    },
  },
  {
    key: 'ecred-aniversario',
    title: 'Aniversário do eCred',
    viewed: false,
    time: 6000,
    thumbnail: {
      title:
        'Mulher negra com blusa verde sorrindo, confetes e balões estão em volta dela.',
      x1: imagemExemplo,
      x2: imagemExemplo,
    },
  },
  {
    key: 'redes-sociais-instagram',
    title: 'Siga nosso Instagram',
    viewed: false,
    time: 5000,
    thumbnail: {
      title:
        'Mulher sentada meditando enquanto ao fundo várias imagens de postagens do Instagram Serasa.',
      x1: imagemExemplo,
      x2: imagemExemplo,
    },
  },
];
