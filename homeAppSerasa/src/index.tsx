import React, {useRef} from 'react';
import {
  NavigationContainer,
  NavigationContainerRef,
} from '@react-navigation/native';
import Routes from './routes';
import navigationService from './services/navigation';

const App = () => {
  const navigationRef = useRef<NavigationContainerRef | null>(null);
  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() => navigationService.setNavigation(navigationRef?.current)}>
      <Routes />
    </NavigationContainer>
  );
};

export default App;
