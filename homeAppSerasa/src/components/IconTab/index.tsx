import React from 'react'

import { selectedIcon } from '../../../res/icons'

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import {colorButtonTab, colorButtonTabSelected} from '../../../res/colors'

import { Container, IsSelected } from './styles'

interface Props {
    selected?: boolean;
    icon: IconProp;
}

const IconTab: React.FC<Props> = ({ selected = false, icon }) => {

    return (
        <>
            {selected ? <IsSelected source={selectedIcon} /> : <></>}
            <Container>

                <FontAwesomeIcon icon={icon} size={20} color={selected ? colorButtonTabSelected : colorButtonTab} />
            </Container>
        </>
    )
}

export default IconTab;