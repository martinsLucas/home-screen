import styled from 'styled-components/native'

export const Container = styled.View`
    height: 100%;
    width: 100%;
    align-items: center;
    padding: 10px;
`;

export const IsSelected = styled.Image`
    margin-top: 0;
    width: 50%;
    height:5px;
`;

export const TitleTab = styled.Text`
    font-size: 10px;
`;