import styled from 'styled-components/native';

export const ContainerStory = styled.View`
  align-items: center;
`;

export const StoryButton = styled.TouchableOpacity`
  width: 80px;
  height: 80px;
  border-radius: 45px;
  border-width: 3px;
  padding: 3px;
  border-color: ${(props) => (props.viewed ? 'black' : 'red')};
`;

export const StoryThumbnail = styled.Image`
  width: 100%;
  height: 100%;
  border-radius: 42px;
`;

export const StoryTitle = styled.Text`
  font-size: 14px;
  text-align: center;
  padding: 5px 10px 5px 10px;
`;
