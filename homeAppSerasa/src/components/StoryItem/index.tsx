import React from 'react';
import {
  ContainerStory,
  StoryButton,
  StoryTitle,
  StoryThumbnail,
} from './styles';
import {Stories} from '../../@types/stories';
import {useNavigation} from '@react-navigation/native';

interface IStoryItemProps {
  story: Stories;
  storyIndex: number;
}

const StoryItem: React.FC<IStoryItemProps> = ({story, storyIndex}) => {
  const navigation = useNavigation();
  return (
    <ContainerStory>
      <StoryButton
        onPress={() => {
          navigation.navigate('StoryScreen', {storyIndex});
        }}>
        <StoryThumbnail
          source={{
            uri: story.thumbnail.x1,
          }}
          accessibilityLabel={story.thumbnail.title}
        />
      </StoryButton>
      <StoryTitle>{story.title}</StoryTitle>
    </ContainerStory>
  );
};

export default StoryItem;
