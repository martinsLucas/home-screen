import React from 'react';
import {FlatList} from 'react-native';
import StoryItem from '../StoryItem';
import {ContainerStories} from './styles';
import {useStoriesSWR} from '../../hooks/stories';

const CardStories = () => {
  const {stories} = useStoriesSWR();
  return stories ? (
    <ContainerStories>
      <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal={true}
        data={stories.list}
        keyExtractor={(item) => item.key}
        renderItem={({item, index}) => (
          <StoryItem story={item} storyIndex={index} />
        )}
      />
    </ContainerStories>
  ) : null;
};

export default CardStories;
