import styled from 'styled-components/native'
import {colorTextTab} from '../../../res/colors'

export const Container = styled.Text`
    font-size: 12px;
    margin: 5px;
    font-weight: bold;
    color: ${colorTextTab}
`;