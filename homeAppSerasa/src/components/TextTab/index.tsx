import  React from 'react'

import {Container} from './styles'

const TextTab = ({children}) => {
    return(
        <Container>
            {children}
        </Container>
    )
}

export default TextTab;