import styled from 'styled-components/native';
import {backgroundColor} from '../../../res/colors'

export const Container = styled.View`
    flex:1;
    background-color: ${backgroundColor}
`;

export const Header = styled.View`
    flex: 1;
    justify-content: space-between;
    flex-direction: row;
    align-items: center;
`;

export const Off = styled.Image`
    width: 30px;
    height: 30px;
    margin: 10px;
`;

export const Content = styled.View`
    align-items: center;
    margin: 30px 30px;
`;

export const IconStatus = styled.Image`
    width: 35px;
    height: 35px;
`;

export const TextStatus = styled.Text`
    margin: 8px 8px;
`;

export const MsgContent = styled.Text`
    font-size: 25px;
`;

export const ContainerProfile = styled.View`
    flex-direction: row;
    align-items: center;
    padding: 10px;
`;

export const ProfileName = styled.Text`

`;
export const TextSituation = styled.Text`
    font-weight: bold;
`;
export const Image = styled.Image`
    background-color: #ccc;
    border-radius: 20px;
    margin: 5px;
`;