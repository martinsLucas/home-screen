import React, {useState, useEffect} from 'react';

import {
  Container,
  ContainerProfile,
  ProfileName,
  Header,
  Content,
  IconStatus,
  MsgContent,
  Image,
  Off,
  TextStatus,
  TextSituation,
} from './styles';
import {StatusBar} from 'react-native';

import Button from '../Button';
import {perfil, off, warning} from '../../../res/icons';
import CardStories from '../CardStories';

const HeaderHome = () => {
  const [statusMsg, setStatusMsg] = useState('');
  const [msg, setMsg] = useState('');

  useEffect(() => {
    setMsg('Você tem');
    setStatusMsg('3 dívidas');
  }, []);

  return (
    <Container>
      <StatusBar backgroundColor="#fff" barStyle="dark-content"></StatusBar>
      <Header>
        <ContainerProfile>
          <Image source={perfil} />
          <ProfileName>Olá Lucas</ProfileName>
        </ContainerProfile>
        <Off source={off} />
      </Header>
      <CardStories />
      <Content>
        <IconStatus source={warning} />
        <TextStatus>Consulta CPF</TextStatus>
        <MsgContent>
          {msg} <TextSituation>{statusMsg}</TextSituation> na Serasa
        </MsgContent>
        <Button onPress={() => console.log('Ola')} text="Ver Mais"></Button>
      </Content>
    </Container>
  );
};

export default HeaderHome;
