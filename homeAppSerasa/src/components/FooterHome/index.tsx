import React from "react";

import {Container, ContainerNews, Title} from './styles'

import {FlatList} from 'react-native'

import CardNews from "../CardNews";

import {news} from '../../../res/images'

const data = [
    {
        id: "abcdefgh",
        imageSource : news ,
        title: "Proteja-se do Coronavírus"
    },
    {
        id: "abcdefghj",
        imageSource : news,
        title: "Economize na quarentena"
    },
    {
        id: "abcdefghjk",
        imageSource : news,
        title: "Renda Extra na quarentena"
    }
]

const FooterHome = ()=>{
    return(
        <Container>
            <Title>Novidades:</Title>
            <ContainerNews>
                <FlatList
                    horizontal={true}
                    data={data}
                    keyExtractor={(item) => item.id}
                    renderItem={({item}) => 
                        <CardNews 
                            sourceImage={item.imageSource}
                            title={item.title}
                        />
                    }
                />
            </ContainerNews>
        </Container>
    )
}

export default FooterHome;