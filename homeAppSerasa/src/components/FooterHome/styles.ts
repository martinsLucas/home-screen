import styled from 'styled-components/native'
import {backgroundColor, colorNovidades} from '../../../res/colors'

export const Container = styled.View`
    background-color: ${backgroundColor};
    padding: 0 30px 0 30px;
    margin-bottom: 20px;

`;

export const ContainerNews = styled.View`
    align-items: flex-start;
`;

export const Title = styled.Text`
    margin: 10px;
    font-weight: bold;
    color: ${colorNovidades}
`;