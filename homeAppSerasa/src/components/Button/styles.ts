import styled from 'styled-components/native';
import {colorButtonVerMais} from '../../../res/colors'
export const ButtonText = styled.Text`
  color: white;
  font-size: 10px;
  text-transform: uppercase;
`;

export const Container = styled.TouchableOpacity.attrs({
  activeOpacity: 0.8,
})`
  background-color: ${colorButtonVerMais};
  padding: 8px 15px 8px 15px;
  border-radius: 20px;
  margin: 20px 20px;
  align-items: center;
`;
