import styled from 'styled-components/native'
import {Animated} from 'react-native'
import { StyleSheet} from 'react-native'

interface Props{
    width: string;
}

export const Container = styled.View`
    height: 10px;
    width: 100%;
    background-color: white;
    border-color: #000;
    border-width: ${StyleSheet.hairlineWidth}px;
    border-radius: 5px;
`;

export const Progress = styled(Animated.View)`
    background-color: #CCC;
`;
