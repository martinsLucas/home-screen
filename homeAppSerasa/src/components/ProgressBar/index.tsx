import React, {useRef, useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';

import {Container, Progress} from './styles';
import {Animated} from 'react-native';
import {StackNavigationProp} from 'react-navigation-stack/lib/typescript/src/vendor/types';

interface Props {
  delay: number;
  exeCallbackFinish?: Function;
  colorProgress?: string;
  colorBackground?: string;
  navigation?: StackNavigationProp<any, any>;
}

const ProgressBar: React.FC<Props> = ({
  delay,
  exeCallbackFinish,
  colorBackground,
  colorProgress,
}) => {
  let animation = useRef(new Animated.Value(0));
  const [progress, setProgress] = useState(0);
  const wait = delay / 100;

  // setInterval custom hook by Dan Abramov
  function useInterval(callback, delay) {
    const savedCallback = useRef(null);

    // Remember the latest callback.
    useEffect(() => {
      savedCallback.current = callback;
    }, [callback]);

    // Set up the interval.
    useEffect(() => {
      function tick() {
        savedCallback.current();
      }
      if (delay !== null) {
        let id = setInterval(tick, delay);
        return () => clearInterval(id);
      }
    }, [delay]);
  }

  useInterval(() => {
    progress < 100 ? setProgress(progress + 1) : exeCallbackFinish(setProgress);
  }, wait);

  // Remember the latest callback.
  useEffect(() => {
    Animated.timing(animation.current, {
      toValue: progress,
      duration: 100,
      useNativeDriver: false,
    }).start();
  }, [progress]);

  const width = animation.current.interpolate({
    inputRange: [0, 100],
    outputRange: ['0%', '100%'],
    extrapolate: 'clamp',
  });

  return (
    <Container>
      <Progress width={width} style={[StyleSheet.absoluteFill]}></Progress>
    </Container>
  );
};

export default ProgressBar;
