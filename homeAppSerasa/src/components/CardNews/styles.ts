import styled from 'styled-components/native'

export const Container = styled.View`
    width: 100px;
    align-items: center;
`;

export const ImageNews = styled.Image`
    width: 80px;
    height: 80px;
    border-radius: 40px;
    background-color: white;
    margin-bottom: 8px;
`;

export const TitleNews = styled.Text`
    font-weight: bold;
    font-size: 12px;
    text-align: center;
    color: #000;
`;