import React from "react";

import {Container, ImageNews, TitleNews} from './styles'
import { ImageSourcePropType } from "react-native";

interface Props{
    sourceImage: ImageSourcePropType;
    title: string; 
}

const CardNews:React.FC<Props> = ({sourceImage, title}) => {
    return(
        <Container>
            <ImageNews source={sourceImage}/>
            <TitleNews>{title}</TitleNews>
        </Container>
    )
}

export default CardNews;