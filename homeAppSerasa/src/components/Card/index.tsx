import React from 'react'

import {
    Container,
    ContainerDesc,
    Description,
    Title,
    ImageCard,
    ContainerImage
} from './styles'

import { ImageSourcePropType } from 'react-native';

interface Props {
    imageSource: ImageSourcePropType;
    title: string;
    content: string;
    backgroundColor?: string;
    colorText?: string;
}

const Card: React.FC<Props> = ({ imageSource, title, content, backgroundColor, colorText }) => {
    return (
        <Container backgroundColor={backgroundColor}>
            <ContainerDesc>
                <Title colorText={colorText}>{title}</Title>
                <Description colorText={colorText}>{content}</Description>
            </ContainerDesc>
            <ContainerImage>
                <ImageCard source={imageSource} />
            </ContainerImage>
        </Container>
    )
}

export default Card;