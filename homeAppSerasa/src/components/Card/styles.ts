import styled from 'styled-components/native'

export const Container = styled.View`
    flex:1;
    padding: 10px;
    margin: 10px;
    background: ${props => {
        const background = props.backgroundColor
        if(background){
            return background
        }else{
            return "#fff"
        }
    }};
    border-radius: 20px;
    flex-direction: row;
    box-shadow: 1px 5px 5px #00f;
    justify-content: space-between;
    align-items:center;
`;

export const ContainerDesc = styled.View`
    width: 50%;
`;

export const Title = styled.Text`
    padding: 10px;
    color: ${props => {
        const color= props.colorText;
        if(color){
            return color;
        }else{
            return "#000"
        }
    }};
`;

export const Description = styled.Text`
    padding: 10px;
    color: ${props => {
        const color= props.colorText;
        if(color){
            return color;
        }else{
            return "#000"
        }
    }};
    font-size: 18px;
    font-weight: bold;
`;

export const ContainerImage = styled.View`
    width: 150px;
    height:150px;
`;
export const ImageCard = styled.Image`
    width: 100%;
    height: 100%;
`;