import {ImageSourcePropType} from 'react-native';

export type StoryList = {
  list: Stories[];
};

export type Stories = {
  key?: string;
  title?: string;
  description?: string;
  viewed?: boolean;
  time?: number;
  highlight?: boolean;
  thumbnail?: Thumbnail;
  media?: Media;
  texts?: Array<string>;
  action?: Action;
  nextStory?: Stories | null;
};

type Thumbnail = {
  title?: string;
  x1?: string;
  x2?: string;
};

type Media = {
  title?: string;
  x1?: string;
  x2?: string;
};

type Action = {
  text?: string;
  action?: string;
  as?: string;
};
