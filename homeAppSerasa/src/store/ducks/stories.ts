import { Stories } from '../../@types/stories'

export const STORIES_SUCCESS_REQUEST = "@stories/STORIES_SUCCESS_REQUEST"

export type StoriesSuccessRequestStories = {
    type: typeof STORIES_SUCCESS_REQUEST;
    payload: {
        message: string;
        status: number;
        data: Array<Stories>
    }
}

export const createSuccessRequestStories = (
    message: string,
    status: number,
    data: Array<Stories>
): StoriesSuccessRequestStories => ({
    type: STORIES_SUCCESS_REQUEST,
    payload: {
        message,
        status,
        data
    }
})

export type Action =
    | StoriesSuccessRequestStories

export type StoriesState = {
    data : Array<Stories>
}

const INITIAL_STATE: StoriesState = {
    data: null
}
const storiesReducer = (state: StoriesState = INITIAL_STATE, action: Action) => {
    switch(action.type){
        case STORIES_SUCCESS_REQUEST:{
            const {message, status, data} = action.payload;
            return {
                ...state,
                data
            };
        }
        default: {
            return state;
        }
    }

}

export default storiesReducer;