import axios from 'axios';

export const api = axios.create({
    baseURL: 'https://k8s-api-cross-dev.ecsbr.net',
    headers: {
        Accept: 'application/json, text/plain, */*',
    },
});