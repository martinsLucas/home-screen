import useSWR from 'swr';

import StoriesService from '../services/stories';

export const useStoriesSWR = () => {
  const {data, mutate, error, isValidating, revalidate} = useSWR(
    '/stories',
    () => {
      return StoriesService.getStoriesList();
    },
  );

  return {
    stories: data?.data,
    mutate,
    error,
    isValidating,
    revalidate,
  };
};
