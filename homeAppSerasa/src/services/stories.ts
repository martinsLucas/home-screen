import {AxiosResponse} from 'axios';

import {Stories, StoryList} from '../@types/stories';
import {api} from '../api';

type DataListResponse = AxiosResponse<{
  message: string;
  status: number;
  data: StoryList;
}>;

const getStoriesList = (): Promise<{
  message: String;
  status: number;
  data: StoryList;
}> => {
  return api
    .get<any, DataListResponse>(
      '/ssw/stories/v1/list/internal/a5931dd29fb6e4aa4c75257145e40cc0',
    )
    .then((response) => response.data)
    .catch((err) => {
      throw err;
    });
};

export default {getStoriesList};
