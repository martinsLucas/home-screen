import {NavigationContainerRef} from '@react-navigation/native';

let navigation: NavigationContainerRef | null = null;

const setNavigation = (navigationRef: NavigationContainerRef | null) => {
  navigation = navigationRef;
};

const navigate = (route: string, params?: object) => {
  navigation?.navigate(route, params);
};

const goBack = () => {
  navigation?.goBack();
};

export default {
  setNavigation,
  navigate,
  goBack,
};
