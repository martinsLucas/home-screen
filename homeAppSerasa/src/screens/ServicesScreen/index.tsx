import React from 'react';

import {Container, Text} from './styles';
import ProgressBar from '../../components/ProgressBar';

const ServiceScreen = () => {
  function executa() {}

  return (
    <Container>
      <Text>Tela Serviço</Text>
      <ProgressBar delay={100000} exeCallbackFinish={executa}></ProgressBar>
    </Container>
  );
};

export default ServiceScreen;
