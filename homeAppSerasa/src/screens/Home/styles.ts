import styled from 'styled-components/native'
import {backgroundColor, backgroundCards} from '../../../res/colors'

export const Container = styled.ScrollView`
    flex: 1;
    background-color: ${backgroundColor}
`;

export const ContainerCards = styled.View`
    flex: 1;
    background-color:${backgroundCards};
    border-top-left-radius: 30px;
    border-top-right-radius: 30px;
    padding: 20px 5px 20px 5px;
`;