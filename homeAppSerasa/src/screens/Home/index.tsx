import React from 'react';

import HeaderHome from '../../components/HeaderHome';
import Card from '../../components/Card';

import {Container, ContainerCards} from './styles';
import {score, cartao} from '../../../res/icons';
import FooterHome from '../../components/FooterHome';

const Home = () => {
  return (
    <Container>
      <HeaderHome />
      <ContainerCards>
        <Card
          title="Serasa Score"
          content="Seu score está baixo"
          imageSource={score}
        />
        <Card
          title="Cartão de crédito"
          content="Solicite um cartão de crédito pelo App"
          imageSource={cartao}
          backgroundColor="#2173d0"
          colorText="white"
        />
      </ContainerCards>
      <FooterHome></FooterHome>
    </Container>
  );
};

export default Home;
