import styled from 'styled-components/native';

export const StoryScreenContainer = styled.View`
  width: 100%;
  height: 100%;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  align-items: center;
  background-color: #f94c4c;
`;

export const StoryTitleContainer = styled.View``;

export const StoryTransparentContainer = styled.View`
  z-index: 5;
  background-color: rgba(0, 0, 0, 0.5);
  width: 100%;
  padding: 10px;
`;

export const StoryTitleText = styled.Text`
  font-size: 28px;
  text-align: center;
  font-weight: bold;
  color: white;
  text-decoration-line: underline;
`;

export const StoryImage = styled.Image`
  position: absolute;
  top: 0px;
  height: 100%;
  width: 100%;
`;
