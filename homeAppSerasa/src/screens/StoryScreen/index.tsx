import React, {useState, useEffect} from 'react';
import {Stories} from '../../@types/stories';
import {
  StoryScreenContainer,
  StoryImage,
  StoryTitleContainer,
  StoryTitleText,
  StoryTransparentContainer,
} from './styles';
import ProgressBar from '../../components/ProgressBar';
import {useStoriesSWR} from '../../hooks/stories';
import {RouteProp, useNavigation, useRoute} from '@react-navigation/native';

interface IStoryProps {
  storyIndex: number;
}

type RouteParams = {
  storyScreen: IStoryProps;
};

const StoryScreen = () => {
  const {stories} = useStoriesSWR();
  const [currentStoryIndex, setStoryIndex] = useState(null);
  const navigation = useNavigation();
  const route = useRoute<RouteProp<RouteParams, 'storyScreen'>>();

  useEffect(() => {
    setStoryIndex(route.params.storyIndex);
  }, [route.params.storyIndex]);

  let story: Stories = stories.list[currentStoryIndex];

  return story ? (
    <StoryScreenContainer>
      <StoryTransparentContainer>
        <ProgressBar
          delay={story.time}
          exeCallbackFinish={(setProgress) => {
            setProgress(0);
            stories.list.length > currentStoryIndex + 1
              ? setStoryIndex(currentStoryIndex + 1)
              : navigation.navigate('Home');
          }}
        />
        <StoryTitleContainer>
          <StoryTitleText>{story.title}</StoryTitleText>
        </StoryTitleContainer>
      </StoryTransparentContainer>
      <StoryImage source={{uri: story.media.x1}} />
    </StoryScreenContainer>
  ) : null;
};

export default StoryScreen;
